﻿using System;

namespace Thermon.Common.Exceptions
{
    public class EntityNotFoundException : Exception
    {
        public string Identifier { get; }

        public EntityNotFoundException(string identifier)
        {
            Identifier = identifier;
        }
    }
}
