﻿using System.Collections.Generic;
using Thermon.Common.Domain.Events;

namespace Thermon.Common.Domain.Entities
{
    public interface IAggregateRoot
    {
        IEnumerable<IDomainEvent> GetPendingEvents();
    }
}
