﻿using System.Collections.Generic;
using Thermon.Common.Domain.Events;

namespace Thermon.Common.Domain.Entities
{
    public abstract class AggregateBase : IAggregateRoot
    {
        private readonly List<IDomainEvent> _domainEvents = new List<IDomainEvent>();

        protected virtual void RaiseEvent<TDomainEvent>(TDomainEvent domainEvent)
            where TDomainEvent : IDomainEvent
        {
            _domainEvents.Add(domainEvent);
        }

        public virtual IEnumerable<IDomainEvent> GetPendingEvents()
        {
            // Return a shallow copy of the Domain Event List to protect the integrity of our private list.
            // Domain Events should be immutable, precluding the need for a deep copy.
            return new List<IDomainEvent>(_domainEvents);
        }
    }
}
