﻿namespace Thermon.Common.Domain.Entities
{
    /// <summary>
    /// The base implementation of a Value Object:
    /// https://enterprisecraftsmanship.com/2017/08/28/value-object-a-better-implementation/
    /// 
    /// Differences between Domain Entities and Value Objects:
    /// https://enterprisecraftsmanship.com/2016/01/11/entity-vs-value-object-the-ultimate-list-of-differences/
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class ValueObjectBase<T>
        where T : ValueObjectBase<T>
    {
        public override bool Equals(object obj)
        {
            var valueObject = obj as T;

            if (ReferenceEquals(valueObject, null))
                return false;

            if (GetType() != obj.GetType())
                return false;

            return EqualsCore(valueObject);
        }

        protected abstract bool EqualsCore(T other);

        public override int GetHashCode()
        {
            return GetHashCodeCore();
        }

        protected abstract int GetHashCodeCore();

        public static bool operator ==(ValueObjectBase<T> a, ValueObjectBase<T> b)
        {
            if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
                return true;

            if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
                return false;

            return a.Equals(b);
        }

        public static bool operator !=(ValueObjectBase<T> a, ValueObjectBase<T> b)
        {
            return !(a == b);
        }
    }
}
