﻿namespace Thermon.Common.Domain.Events
{
    /// <summary>
    /// Marker Interface for Domain Events
    /// 
    /// Objects that derive from this interface <b>must be</b> immutable
    /// </summary>
    public interface IDomainEvent
    {
    }
}
