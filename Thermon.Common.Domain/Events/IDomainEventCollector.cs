﻿using System.Collections.Generic;

namespace Thermon.Common.Domain.Events
{
    public interface IDomainEventCollector
    {
        void Collect(IDomainEvent domainEvent);
        void Collect(IEnumerable<IDomainEvent> domainEvents);
    }
}
