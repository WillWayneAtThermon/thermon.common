﻿using System.Threading.Tasks;

namespace Thermon.Common.Domain.Publishers
{
    /// <summary>
    /// Handles events that should occur before committing changes
    /// </summary>
    public interface IDomainEventPreCommitPublisher
    {
        Task DispatchEventsAsync();
    }
}
