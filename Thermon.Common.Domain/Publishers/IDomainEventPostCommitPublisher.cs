﻿using System.Threading.Tasks;

namespace Thermon.Common.Domain.Publishers
{
    /// <summary>
    /// Handles events that should occur after committing changes, such as persisting Domain Events
    /// </summary>
    public interface IDomainEventPostCommitPublisher
    {
        Task DispatchEventsAsync();
    }
}
