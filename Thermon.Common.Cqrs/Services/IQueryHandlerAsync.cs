﻿using System.Threading;
using System.Threading.Tasks;

namespace Thermon.Common.Cqrs.Services
{
    public interface IQueryHandlerAsync<in TQuery, TReturn> where TQuery : IQuery
    {
        Task<TReturn> HandleAsync(TQuery query, CancellationToken cancellationToken = default(CancellationToken));
    }
}
