﻿using System.Threading;
using System.Threading.Tasks;

namespace Thermon.Common.Cqrs.Services
{
    public interface ICommandHandlerAsync<in TCommand> where TCommand : ICommand
    {
        Task HandleAsync(TCommand command, CancellationToken cancellationToken = default(CancellationToken));
    }
}
