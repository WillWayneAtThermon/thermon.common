﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace Thermon.Common.Data.InMemory
{
    /// <summary>
    /// The Base Class for In-Memory Repositories.
    /// In-Memory Repositories are not intended for Production. Rather, they are useful for Testing and early substitutes for a proper data layer.
    /// Inheritors must specify what the ID of each entity will be (must be IComparable), and the type of the object to be stored.
    ///
    /// All CRUD methods (+ UpsertAsync ... CRUUD?) are protected to allow the implementer to expose methods as needed.
    /// </summary>
    /// <typeparam name="TId">The type of the entity's unique identifier. (must be IComparable)</typeparam>
    /// <typeparam name="TEntity">The type of the Entity to be managed in the store.</typeparam>
    public abstract class RepositoryBase<TId, TEntity>
        where TId : IComparable
    {
        /// <summary>
        /// Perform a CreateAsync operation with the entity given.
        /// </summary>
        /// <param name="id">The ID of the new Entity</param>
        /// <param name="newEntity">The new Entity</param>
        /// <exception cref="Exception">Occurs when an entity with the given key already exists.</exception>
        protected async Task CreateAsync(TId id, TEntity newEntity)
        {
            if (InternalDictionary.ContainsKey(id))
                throw new Exception($"Entity with key [{id}] already exists");

            await UpsertAsync(id, newEntity);
        }

        /// <summary>
        /// Perform a ReadAsync operation given an ID.
        /// </summary>
        /// <param name="id">The ID of the entity to find.</param>
        /// <returns>Returns the found entity.</returns>
        /// <exception cref="EntityNotFoundException">Thrown when no entity with the given ID exists.</exception>
        protected Task<TEntity> ReadAsync(TId id)
        {
            if (!InternalDictionary.ContainsKey(id))
            {
                throw new EntityNotFoundException(id.ToString());
            }

            // Perform a deep clone to prevent modification directly to the stored object
            var returnValue = Clone(InternalDictionary[id]);

            return Task.FromResult(returnValue);
        }

        /// <summary>
        /// Performs an UpdateAsync operation to an existing entity.
        /// </summary>
        /// <param name="id">The ID of the Entity to UpdateAsync</param>
        /// <param name="updatedEntity">The updated Entity</param>
        /// <exception cref="EntityNotFoundException">Thrown when no entity with the given ID exists to be updated.</exception>
        protected async Task UpdateAsync(TId id, TEntity updatedEntity)
        {
            if (!InternalDictionary.ContainsKey(id))
                throw new EntityNotFoundException($"{id}");

            await UpsertAsync(id, updatedEntity);
        }

        /// <summary>
        /// Performs an UpsertAsync operation against the internal storage
        /// </summary>
        /// <param name="id">The ID of the new or updated Entity</param>
        /// <param name="newOrUpdatedEntity">The new or updated Entity</param>
        protected Task UpsertAsync(TId id, TEntity newOrUpdatedEntity)
        {
            InternalDictionary.AddOrUpdate(id,
                newOrUpdatedEntity, // If New
                (existingId, entity) => newOrUpdatedEntity); // If existing.

            return Task.FromResult(0);
        }

        /// <summary>
        /// Remove an entity from the internal storage
        /// </summary>
        /// <param name="id">The ID of the object to remove</param>
        protected Task DeleteAsync(TId id)
        {
            InternalDictionary.TryRemove(id, out _);

            return Task.FromResult(0);
        }

        /// <summary>
        /// Clones an entity into a new instance of that entity.
        ///
        /// This is used by the ReadAsync method to prevent users from getting and modifying the state of a stored entity without needing to Use the UpdateAsync/UpsertAsync methods.
        /// </summary>
        /// <param name="existingEntity">The Entity that comes from the Database</param>
        /// <returns>A deep clone of the Entity input.</returns>
        protected abstract TEntity Clone(TEntity existingEntity);

        private static readonly ConcurrentDictionary<TId, TEntity> InternalDictionary = new ConcurrentDictionary<TId, TEntity>();
    }
}
